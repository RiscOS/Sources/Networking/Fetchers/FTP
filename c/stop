/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * FTP (c.stop)
 *
 * � Acorn Computers Ltd. 1996-1997
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kernel.h"
#include "swis.h"
#include "module.h"
#include "protocol.h"


/*************************************************************/
/* _kernel_oserror ftp_stop(_kernel_swi_regs *r)	     */
/*************************************************************/
/* The call to stop getting data from a site. The values in  */
/* the registers are:					     */
/*		       r0 = Flags word			     */
/*		       r1 = pollword of client		     */
/* On exit, R0 is the protocol status word (we use 0)	     */
/*************************************************************/
_kernel_oserror *ftp_stop(_kernel_swi_regs *r)
{
	Session *ses;

	r->r[0] = 0;
	ses = find_session(r->r[1]);

	#ifdef TRACE
	protocol_debug("ftp_stop for session %p\n", ses);
	#endif

	if (ses != NULL) {
		if (ses->data_sd != -1) {
		        _kernel_swi_regs r2 = *r;
			ftp_abort(ses, &r2);
		}
		new_connection(ses);
		kill_session(ses);
	}

	return NULL;
}
